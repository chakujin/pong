﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cuadrados : MonoBehaviour
{
   // private int dmg=0;
    public int life;

    void OnCollisionEnter2D(Collision2D other)
    {
        
        if (other.gameObject.tag == "Ball")
        {

            if (life <= 0)
            {                
                Destroy(gameObject);
            }
            else {
                life--;
            }

        }
    }
}

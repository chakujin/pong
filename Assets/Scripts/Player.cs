﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    public float velocidadY;
    public float maxY;
    private float posY;
    private float direction;

    // Update is called once per frame
    void Update()
    {

        direction = Input.GetAxis("Horizontal");

        posY = transform.position.x + direction * velocidadY * Time.deltaTime;

        if (posY > maxY)
        {
            posY = maxY;
        }
        else if (posY < -maxY)
        {
            posY = -maxY;
        }

        transform.position = new Vector3(posY,transform.position.y, transform.position.z);
    }
}

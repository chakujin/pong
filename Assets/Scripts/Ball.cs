﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Ball : MonoBehaviour
{
    private Vector2 ballPosition;
    public Vector2 ballVelocity;
    public int life;
    public Text lifeText;
    public GameObject GameOver;


    void Awake()
    {
        ballPosition = transform.position;
    }

    private void Start()
    {
        lifeText.text = "Lifes:" + life;
    }

    // Update is called once per frame
    void Update()
    {
        ballPosition.y = transform.position.y + ballVelocity.y * Time.deltaTime;
        ballPosition.x = transform.position.x + ballVelocity.x * Time.deltaTime;

        transform.position = new Vector3(ballPosition.x, ballPosition.y, transform.position.z);
    }

    
    void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.tag == "Muro")
            {
                ballVelocity.x *= -1;
            }
        if (other.gameObject.tag == "Cuadrados")
            {

            if (other.contacts[0].normal.x < other.contacts[0].normal.y && other.contacts[0].normal.x < 0)  //(1,0)
            {
                ballVelocity.x *= -1;

            } else if(other.contacts[0].normal.x > other.contacts[0].normal.y && other.contacts[0].normal.x > 0)
            {
                ballVelocity.x *= -1;

            } else if (other.contacts[0].normal.y < other.contacts[0].normal.x && other.contacts[0].normal.y < 0)  //(1,0)
            {
                ballVelocity.y *= -1;

            }
            else if (other.contacts[0].normal.y > other.contacts[0].normal.x && other.contacts[0].normal.y > 0)
            {
                ballVelocity.y *= -1;

            }

        }
        if (other.gameObject.tag == "Murofinal")
            {
                ballVelocity.x *= -1;
            transform.position = new Vector3(0,0,0);
            life--;
            lifeText.text = "Lifes:" + life;
            if (life <= 0)
                {
                GameOver.SetActive(true);
                Destroy(gameObject);
                
                }
            }
        else if (other.gameObject.tag == "Pala")
        {
            ballVelocity.y *= -1;
        }
    }


}